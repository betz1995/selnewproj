package framework.api;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import bl.framework.base.Browser;
import bl.framework.base.Element;
import utils.Reporter;

public class Selbase extends Reporter implements Browser,Element  {
	public RemoteWebDriver driver;
	WebDriverWait wait;
	int i=1;
	
		@Override
		public void startApp(String browser, String url) {
			try {
				if(browser.equalsIgnoreCase("chrome")) {
					System.setProperty("webdriver.chrome.driver",
							"./drivers/chromedriver.exe");
					driver = new ChromeDriver();
				} else if(browser.equalsIgnoreCase("firefox")) {
					System.setProperty("webdriver.gecko.driver",
							"./drivers/geckodriver.exe");
					driver = new FirefoxDriver();
				} else if(browser.equalsIgnoreCase("ie")) {
					System.setProperty("webdriver.ie.driver",
							"./drivers/IEDriverServer.exe");
					driver = new InternetExplorerDriver();
				}
				driver.navigate().to(url);
				driver.manage().window().maximize();
				driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			} catch (Exception e) {
				System.err.println("The Browser Could not be Launched. Hence Failed");
				throw new RuntimeException();
			} finally {
				takeSnap();
			}

		}

	

@Override
public WebElement locateElement(String locatorType, String value) {
	try {
		switch(locatorType.toLowerCase()) {
		case "id": return driver.findElementById(value);
		case "name": return driver.findElementByName(value);
		case "class": return driver.findElementByClassName(value);
		case "link": return driver.findElementByLinkText(value);
		case "xpath": return driver.findElementByXPath(value);
		}
	} catch (NoSuchElementException e) {
		System.err.println("The Element with locator:"+locatorType+" Not Found with value: "+value);
		throw new RuntimeException();
	}
	return null;
}





@Override
public List<WebElement> locateElements(String type, String value) {
	try {
		switch(type.toLowerCase()) {
		case "id": return driver.findElementsById(value);
		case "name": return driver.findElementsByName(value);
		case "class": return driver.findElementsByClassName(value);
		case "link": return driver.findElementsByLinkText(value);
		case "xpath": return driver.findElementsByXPath(value);
		}
	} catch (NoSuchElementException e) {
		System.err.println("The Element with locator:"+type+" Not Found with value: "+value);
		throw new RuntimeException();
	}
	return null;
}


@Override
public void click(WebElement ele) {
	try {
		wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(ele));
		ele.click();
		logStep("The Element "+ele+" clicked", "pass"); 
	} catch (StaleElementReferenceException e) {
		logStep("The Element "+ele+" could not be clicked", "fail");
		throw new RuntimeException();
	} finally { 
		takeSnap();
	}
	

}

@Override
public void clearAndType(WebElement ele, String data) {
	try {
		ele.clear();
		ele.sendKeys(data);
		logStep("The Data :"+data+" entered Successfully", "pass");
	} catch (ElementNotInteractableException e) {
		logStep("The Element "+ele+" is not Interactable", "fail");
		throw new RuntimeException();
	}finally {
		takeSnap();
	}

}



@Override
public void append(WebElement ele, String data) {
	// TODO Auto-generated method stub
	
}





@Override
public void clear(WebElement ele) {
	// TODO Auto-generated method stub
	
}





@Override
public String getElementText(WebElement ele) {
	// TODO Auto-generated method stub
	return null;
}





@Override
public String getBackgroundColor(WebElement ele) {
	// TODO Auto-generated method stub
	return null;
}





@Override
public String getTypedText(WebElement ele) {
	// TODO Auto-generated method stub
	return null;
}





@Override
public void selectDropDownUsingText(WebElement ele, String value) {
	// TODO Auto-generated method stub
	
}





@Override
public void selectDropDownUsingIndex(WebElement ele, int index) {
	// TODO Auto-generated method stub
	
}





@Override
public void selectDropDownUsingValue(WebElement ele, String value) {
	// TODO Auto-generated method stub
	
}





@Override
public boolean verifyExactText(WebElement ele, String expectedText) {
	// TODO Auto-generated method stub
	return false;
}





@Override
public boolean verifyPartialText(WebElement ele, String expectedText) {
	// TODO Auto-generated method stub
	return false;
}





@Override
public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
	// TODO Auto-generated method stub
	return false;
}





@Override
public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
	// TODO Auto-generated method stub
	
}





@Override
public boolean verifyDisplayed(WebElement ele) {
	// TODO Auto-generated method stub
	return false;
}





@Override
public boolean verifyDisappeared(WebElement ele) {
	// TODO Auto-generated method stub
	return false;
}





@Override
public boolean verifyEnabled(WebElement ele) {
	// TODO Auto-generated method stub
	return false;
}





@Override
public boolean verifySelected(WebElement ele) {
	// TODO Auto-generated method stub
	return false;
}





@Override
public void startApp(String url) {
	// TODO Auto-generated method stub
	
}





@Override
public WebElement locateElement(String value) {
	// TODO Auto-generated method stub
	return null;
}





@Override
public void switchToAlert() {
	// TODO Auto-generated method stub
	
}





@Override
public void acceptAlert() {
	// TODO Auto-generated method stub
	
}





@Override
public void dismissAlert() {
	// TODO Auto-generated method stub
	
}





@Override
public String getAlertText() {
	// TODO Auto-generated method stub
	return null;
}





@Override
public void typeAlert(String data) {
	// TODO Auto-generated method stub
	
}





@Override
public void switchToWindow(int index) {
	// TODO Auto-generated method stub
	
}





@Override
public void switchToWindow(String title) {
	// TODO Auto-generated method stub
	
}





@Override
public void switchToFrame(int index) {
	// TODO Auto-generated method stub
	
}





@Override
public void switchToFrame(WebElement ele) {
	// TODO Auto-generated method stub
	
}





@Override
public void switchToFrame(String idOrName) {
	// TODO Auto-generated method stub
	
}





@Override
public void defaultContent() {
	// TODO Auto-generated method stub
	
}





@Override
public boolean verifyUrl(String url) {
	// TODO Auto-generated method stub
	return false;
}





@Override
public boolean verifyTitle(String title) {
	// TODO Auto-generated method stub
	return false;
}





@Override
public long takeSnap() {
	long number = (long) Math.floor(Math.random() * 900000000L) + 10000000L; 
	try {
		FileUtils.copyFile(driver.getScreenshotAs(OutputType.FILE) , new File("./reports/images/"+number+".jpg"));
	} catch (WebDriverException e) {
		System.out.println("The browser has been closed.");
	} catch (IOException e) {
		System.out.println("The snapshot could not be taken");
	}
	return number;
}



@Override
public void close() {
	// TODO Auto-generated method stub
	
}





@Override
public void quit() {
	// TODO Auto-generated method stub
	
}
}










